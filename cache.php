<?php
/* USAGE:
boost()->cache->enable(true);
Use either at the top of the app file, or inside of certain routes.
*/
namespace Boost;

boost()->add_callable('cache', 'Boost\Cache', true);

Class Cache extends Library {

	private $cache;
	private $filename_hash;
	private $cache_enabled = false;

	function ___boost_init() {
		$this->cache = new \Gregwar\Cache\Cache;
		$this->cache->setActualCacheDirectory(ROOTPATH.'/cache');
		$this->cache->setPrefixSize(0);
		$this->filename = md5(boost()->url->full_url()).'.cache';

		boost()->hook->into('boost-app-before', function() {
			boost()->cache->boost_app_before();
		}, -100000);

		boost()->hook->into('boost-app-after', function() {
			boost()->cache->boost_app_after();
		}, 100000);
	}

	function boost_app_after() {
		$page_content = ob_get_clean();
		if ($this->cache_enabled) {
			$this->cache->set($this->filename, $page_content);
		}
		echo $page_content;
	}

	function boost_app_before() {
		$this->output_if_valid();
		ob_start();
	}

	function clear() {
		$dir = ROOTPATH.'/'.$this->cache->getCacheDirectory();
		$this->rrmdir($dir);
	}

	private function rrmdir($dir) {
		if (is_dir($dir)) {
			$objects = scandir($dir);
			foreach ($objects as $object) {
				if ($object != "." && $object != "..") {
					if (filetype($dir."/".$object) == "dir") rmdir($dir."/".$object); else unlink($dir."/".$object);
				}
			}
			reset($objects);
			rmdir($dir);
		}
	}

	private function output_if_valid() {
		if ($this->cache_enabled) {
			$existing_cache_contents = $this->cache->get($this->filename, array(
				'max-age' => 3600
			));
			if ($existing_cache_contents) {
				echo $existing_cache_contents;
				exit;
			}
		}
	}

	function enable() {
		$this->cache_enabled = true;
		$this->output_if_valid();
	}

}